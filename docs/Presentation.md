This resource is used to contain information regarding a presentation. The presentation topic is Authentication & Authorization and how to apply our developed system to services.

### Authentication:
- Authentication is the means by which you confirm your identity. In our architecture this will be done by utilizing Keycloak's hosted login page. Our application will utilize (for now) Keycloak’s prebuilt login page which is host by the service itself. We have chosen to use [Authorization Code Flow]( https://auth0.com/docs/flows/guides/auth-code/add-login-auth-code) to handle securely Authorizing users.

### Sequence Diagrams:
A Sequence Diagram can be seen [here]( https://sequencediagram.org/index.html#initialData=C4S2BsFMAIEEFdgAsD2AnEAvAhqFA7aAYRQBMYAxcFAdwCg7wQAzSJ-SAYxWrWgFUAzpD4BiAEbhsnANaMWbEB269oAdUjjoAZREA3EJxgTw8SPNbsuPdNADSkAJ6dq2GdFEBzNJEj4GQiIAtAB8Glq6aAZGAFzETLLQADIonkrJSnLhOvqGkKEOzq4ycbD8yOhYuCAExGQwAEqQAI5mgsDQwCjQAPTYiKgYmOaFLihuoYFocU2kID6cHV3JqUo9CBVD1bUACmgoALYADsB0UwVOY26lA36gnLgw2PikdfjC+KejxaHZkdGQG6bKp4Qgkch0P65IwXIrjEpwAboHCgurkaAAaniIDu0AAkgARTHY3G6Tg+JbdHrjAY9LoyPx0b7w2FXBEANWwTFIj0RwJRNUI3HRWKITFxhOJYpxnxy5MgX0uPzCmhyUTycUlABUUAzCM9XrBOEZBIJoDq9ZDVf88qFYDs8TMWm0OjzgNhoDQwEg4MbIGaLYz7XjftboYDoE0jgRhHQgA). This sequence diagram covers the entire Authorization Code Flow which will be utilized at the gateway to perform Authentication and Authorization of users and routes respectively.

This Sequence [Diagram](https://sequencediagram.org/index.html#initialData=C4S2BsFMAICECcD2B3Azpe0Aqi4EMBjAa0gDsATAKEvBADNJbTIDFxFMBhWs4aAYgBG4QkRr1GIZq3aYA4nmCRkeAJ4D4kKrQZMWbDtADSkVQXZ4iGrdW4heAWgB8AZQwA3EAUgAuaACVIAEcAV0hUYEo3eE9vZ2jY32gASVRoAEEQ4AALXi9FLQB+KI8vSGcTMwsiP2S6aAA5AHksDKzc0lACAvJoTXIQTQJIhLL40u9atMycjhAALyKSmLGnO15a+tJEPhnsucXezWAQ+FJoOg5BEHJyMgAKABYABgBmAEoAOhT6vYOtPqQE5naDkRR4T5AA) demonstrates what each REst API is responsible for implementing on their service.

### Json Web Token:
- A Json Web Token (JWT) is a self-contained way for securely transmitting information between parties as a JSON object. This information can be verified and trusted because it is digitally signed. JWTs can be signed using a secret (with the HMAC algorithm) or a public/private key pair using RSA or ECDSA.
- A JWT is comprised of three pieces. These pieces are a Header, Payload, and Signature.

1.   The header will contain two fields 'alg' and 'typ'. For our purpose we are currently using HS256 and JWT respectively.
2.   The Payload can contain a variety of information. This information can be about the token, a user, and anything else the Oauth server determined useful or needed. An example of a token used in our application is here:
     