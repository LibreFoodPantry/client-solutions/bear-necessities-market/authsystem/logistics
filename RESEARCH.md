# Research Archive
Note: These resources can be utilized to gain an understanding of OAuth2, Spring Security, and their application to securing a MicroServices Architecture. 

## OAuth 2.0
### What is OAuth 2.0
[OAuth2](https://oauth.net/2/ "Link to OAuth2 HomePage") is an authorization framework that enables applications to obtain limited access to user accounts on an HTTP service. It delegates user authentication to the service that host the user account and authorizaing third-party applications to access the user account. 
<br>
A brief [video](https://www.youtube.com/watch?v=CPbvxxslDTU&t=28s "A Video about OAuth2.0") explaining how OAuth2.0 works

#### Resources
 - [OAuth2 Homepage](https://oauth.net/2/ "Link to OAuth2 HomePage")
 - [OAuth2.0 Framwork Proposed Standard - RFC6749](https://tools.ietf.org/html/rfc6749 "RFC Proposed Standard")
 - [A good explanation of OAuth2.0 Terminology](https://aaronparecki.com/oauth-2-simplified/#creating-an-app)
 - [OAuth2.0 Example Access Token Usage](https://www.digitalocean.com/community/tutorials/an-introduction-to-oauth-2#example-access-token-usage)
 - [OAuth2.0 Code Example](https://www.ibm.com/developerworks/library/se-oauthjavapt2/index.html "Example by IBM") By IBM Developerworks Library
 - [OAuth2.0 Code Example](https://developer.byu.edu/docs/consume-api/use-api/oauth-20/oauth-20-java-sample-code "Example by BYU") By University of BYU
 - [OAuth Guide](https://auth0.com/docs/quickstart/backend/java-spring-security5#configure-the-sample-project) By OAuth Team

#### License
 - OAuth2.0 uses the: [Apache License](https://www.apache.org/licenses/)

## Spring Cloud Gateway
### What is Spring Cloud Gateway
The [Spring.io](https://spring.io/projects/spring-cloud-gateway) Website defines it as:
> This project provides a library for building an API Gateway on top of Spring MVC. Spring Cloud Gateway aims to provide a simple, yet effective way to route to APIs and provide cross cutting concerns to them such as: security, monitoring/metrics, and resiliency.

A [video](https://www.youtube.com/watch?v=jOawuL1Xnwo&t=179s "Video about Spring Cloud Gateway") about Spring Cloud Gateway by a Pivatol Developer

#### Resources
 - [Spring Cloud Gateway Homepage](https://spring.io/projects/spring-cloud-gateway)
 - [Project Reactor](https://projectreactor.io/learn) Spring Cloud Gateway utilizes Project Reactor
 - This [medium](https://medium.com/tech-tajawal/microservice-authentication-and-authorization-solutions-e0e5e74b248a) and [Smartbear](https://smartbear.com/learn/api-design/api-gateways-in-microservices/) articles make a good case for a gateway architecture
 - [Spring Gateway Code Example](https://spring.io/blog/2019/08/16/securing-services-with-spring-cloud-gateway)
 - [Spring Cloud Gateway Github](https://github.com/spring-cloud/spring-cloud-gateway)
 - [Good Video Example](https://www.youtube.com/watch?v=NkgooKSeF8w&t=310s) by a Pivatol Developer
 - [A basic example of Gateway](https://spring.io/guides/gs/gateway/#_what_youll_need "Spring.io Guide")


### Utilization:

## OpenID
### What is OpenID
[OpenId.net](https://openid.net/connect/) describes it as:
> OpenID Connect 1.0 is a simple identity layer on top of the OAuth 2.0 protocol. It allows Clients to verify the identity of the End-User based on the authentication performed by an Authorization Server, as well as to obtain basic profile information about the End-User in an interoperable and REST-like manner.

### Resources
- [OpenId HomePage](https://openid.net/connect/)
- [RFC 6749](https://tools.ietf.org/html/rfc6749#section-4.1)
- [OpenID Connect Tutorial](https://developers.onelogin.com/openid-connect)

### Utilization:

### License
[License](https://openid.net/intellectual-property/copyright-license/)

## Json Web Token (JWT)
### What is a JWT

### Resources

### Utilization

### Data Flow Diagrams
#### Spring Cloud Gateway + Identify Provider
![Image](images/Flow_with_Identify_provider.PNG)

#### Identity Progagation
![](images/Identity_Propagation.PNG)
