## Documentation

#### BNM Configured Project

A BNM pre-configured project will be included under [UserModule-BNM](https://gitlab.com/LibreFoodPantry/modules/usermodule-bnm). This project will include a pre-configured realm, services that were ready to be added, and any additional required configuration to get the service functioning properly.

**Note:** To quickly gain an understanding of what a Realm and User are you can utilize the [Creating a Realm and User](https://www.keycloak.org/docs/latest/getting_started/#creating-a-realm-and-user) document published by the Keycloak Organization.

### How to Start

Under the standard project, you will need to correctly configure a Keycloak Realm for your use case. To correctly configure a realm for your need I would suggest reading the Keycloak [Server Administration Documentation](https://www.keycloak.org/docs/latest/server_admin/#overview).

#### BNM Configured Project

The recommend steps for the [BNM Project](https://gitlab.com/LibreFoodPantry/modules/usermodule-bnm/BNM) are as follows:

1.  Clone the [BNM Project](https://gitlab.com/LibreFoodPantry/modules/usermodule-bnm/BNM).
2.  Verify that the [BNM-Market.json]() is located under [Realms]().
3.  Move the Realm to a local directory that is mounted for this Project.
4.  Ensure that the [Docker-Compose.yaml]() Keycloak `volumes:` has the correct path to the directory you are utilizing.
5.  Navigate to the BNM Project local directory and run `Docker-compose up` and ensure it builds with no errors. **Note:** Check your mounted volume for a folder named `hsperfdata_jboss` if this folder does not exist the most likely reason is Docker is not correctly configured to mount local volumes. If you are using Docker Toolbox I recommend this [guide](https://headsigned.com/posts/mounting-docker-volumes-with-docker-toolbox-for-windows/).
6.  Navigate to `[docker hostname/address]:[Gateway Port]/auth` the default URL is `192.168.99.100:9500/auth` and ensure you connect to the Keycloak landing page.
7.  Login and with the Default Admin Username and Password. Verify the BNM-Realm is present on the top left under Realms.

If you follow these steps and everything works you should be able to utilize the project. If there are services already added you can ensure they are correctly functioning by navigating to each endpoint via `192.168.99.100:9500/[Path]`. I recommend testing secured paths to ensure they are correctly denying access to clients who are not Authenticated or Authorized.

### How to Shutdown

### How to add a new Service

### How to Integrate a new Service

**note to self: discuss static files**

---

## Additional Resources

All Research done for this project can be found here: [Research](RESEARCH.md)

---

Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
